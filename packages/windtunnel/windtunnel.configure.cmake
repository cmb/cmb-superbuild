message(STATUS "Configuring modulebuild+windtunnel package")
set_property(GLOBAL PROPERTY "${SUPERBUILD_PACKAGE_MODE}_REQUIRED_PROJECTS" "smtkwindtunnel;cmb;smtk")
set_property(GLOBAL PROPERTY "${SUPERBUILD_PACKAGE_MODE}_EXCLUDE_PROJECTS" "")

include(SuperbuildVersionMacros)
superbuild_set_version_variables(smtkwindtunnel "1.0.0" "smtkwindtunnel-version.cmake" "version.txt")

set(package_extra_projects
  smtkwindtunnel
)
list(APPEND superbuild_extra_package_projects "${package_extra_projects}")

list(APPEND smtk_plugin_omit
  smtkDelaunayPlugin
  smtkMeshSessionPlugin
  smtkOscillatorSessionPlugin
  smtkPQProjectPlugin
  smtkPolygonSessionPlugin)
set_property(GLOBAL PROPERTY smtk_plugins_omit ${smtk_plugin_omit})
set_property(GLOBAL APPEND PROPERTY cmb_extra_plugins openfoam-windtunnel)
set_property(GLOBAL APPEND PROPERTY cmb_extra_dependencies smtkwindtunnel)

# Configure Options
set(ENABLE_smtkwindtunnel ON)
set(ENABLE_cmb ON)
set(ENABLE_cmbworkflows OFF)
set(ENABLE_smtkresourcemanagerstate OFF)

set_property(GLOBAL APPEND PROPERTY smtk_extra_package_cmake_arguments
  -DSMTK_ENABLE_OPERATION_THREADS:BOOL=OFF)

if (DEVELOPER_MODE_smtk)
  message(FATAL_ERROR "Cannot build in windtunnel packaging mode if DEVELOPER_MODE_smtk is ON")
endif ()

include(CMBBundleMacros)
cmb_generate_package_suffix(${SUPERBUILD_PACKAGE_MODE})
cmb_generate_package_bundle(${SUPERBUILD_PACKAGE_MODE}
  PACKAGE_NAME "modelbuilder-${SUPERBUILD_PACKAGE_MODE}"
  DESCRIPTION "CMB + WindTunnel"
  PACKAGE_VERSION smtkwindtunnel
  HAS_WORKFLOWS
  EXCLUDE_VERSION
  )
